1. To run the cucumber test execute the below command
`mvn test`
2. To generate the report execute the below command
`mvn -Dtest=Report#generateReport test`
3. Report will be genrated at the below path 
`target/cucumber/report/cucumber-html-reports/overview-features.html`