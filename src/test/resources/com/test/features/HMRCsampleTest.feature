Feature: Selecting highest price item

  @highestPrice
  Scenario: Verify the user can select the highest price item and add to the cart

    Given user navigated to the automationpractice url
    When user clicks on dresses menu
    And selected the highest price item and added to the cart
    Then verify the selected item is added to the cart
