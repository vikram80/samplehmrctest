package com.test;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        plugin = { "pretty", "html:target/cucumber.html",
                "json:target/cucumber.json",
                "junit:target/cucumber.xml",
                },
        glue = { "classpath:com/test/steps", "classpath:com/test/configs/hooks" },
        features = "classpath:com/test/features")
public class TestRunner {

}
