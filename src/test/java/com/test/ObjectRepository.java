package com.test;

import com.test.pages.HomePage;
import com.test.pages.Navigate;
import com.test.pages.DressesPage;

public class ObjectRepository {

    private HomePage homePage;
    private Navigate navigate;
    private DressesPage dressesPage;

    public ObjectRepository() {
        //Empty constructor
    }

    protected HomePage homePage() {

        if (homePage == null) {
            homePage = new HomePage();
        }
        return homePage;
    }

    protected Navigate navigate() {

        if (navigate == null) {
            navigate = new Navigate();
        }
        return navigate;
    }

    protected DressesPage dressesPage() {

        if (dressesPage == null) {
            dressesPage = new DressesPage();
        }
        return dressesPage;
    }
}
