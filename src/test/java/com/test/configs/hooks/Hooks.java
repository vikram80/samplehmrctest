package com.test.configs.hooks;

import com.test.configs.WebDriverManager;
import io.cucumber.java.After;
import org.openqa.selenium.WebDriver;

public class Hooks {


    @After
    public void tearDown() {
        WebDriverManager webDriverManager = new WebDriverManager();
        WebDriver driver = webDriverManager.getDriver();
        driver.quit();
    }
}
