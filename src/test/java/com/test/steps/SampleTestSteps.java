package com.test.steps;

import com.test.ObjectRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class SampleTestSteps extends ObjectRepository {

    String highestPrice = null;

    @Given("user navigated to the automationpractice url")
    public void userNavigatedToTheAutomationpracticeUrl() {
        navigate().toAutomationPractice();
    }

    @When("user clicks on dresses menu")
    public void userClicksOnDressesMenu() {
        homePage().clickOnDressesLink();
    }

    @And("selected the highest price item and added to the cart")
    public void selectedTheHighestPriceItemAndAddedToTheCart() {
        highestPrice = dressesPage().getHighestprice();
        dressesPage().clickOnHighestPriceItem();
    }

    @Then("verify the selected item is added to the cart")
    public void verifyTheSelectedItemIsAddedToTheCart() throws InterruptedException {
        assertThat(dressesPage().getHeader().trim(), is(equalTo("Product successfully added to your shopping cart")));
        assertThat(dressesPage().getPriceOnCart().trim(), is(equalTo("$"+highestPrice)));
    }
}
