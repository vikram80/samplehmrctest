package com.test.pages;

import com.test.configs.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.stream.Collectors;

public class DressesPage extends UtilPage {

    private By prices = By.cssSelector("span[class='price product-price']");
    private By addCart = By.cssSelector("a[title='Add to cart']");
    private By cartPrice = By.cssSelector("span[id='layer_cart_product_price']");
    private By header = By.cssSelector("h2");

    private WebDriverManager webDriverManager = new WebDriverManager();

    public void clickOnHighestPriceItem() {
        List<Double> prices = getPrices();
        Double highestprice = prices.stream().max(Double::compareTo).get();
        int index = prices.indexOf(highestprice);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElements(this.prices).get(index+2)).perform();
        getElements(addCart).get(index).click();
    }

    public String getHighestprice() {
        List<Double> prices = getPrices();
        return prices.stream().max(Double::compareTo).get().toString();
    }

    private List<Double> getPrices() {
        return getElementsText(this.prices).stream()
                .filter(str-> str.length()>0)
                .map(str -> str.replace("$", ""))
                .map(Double::parseDouble)
                .collect(Collectors.toList());
    }

    public String getHeader() throws InterruptedException {
        for (int i = 0; i < 30; i++) {
            if(getElementText(header).length()==0)
                Thread.sleep(1000);
            else
                break;
        }
        return getElementText(header);
    }

    public String getPriceOnCart() {
        return getElementText(cartPrice);
    }
}
