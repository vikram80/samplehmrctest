package com.test.pages;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Report {
    @Test
    public void generateReport() {
        File reportOutput = new File("target/cucumber/report");
        List<String> jsonfiles = new ArrayList<>();
        jsonfiles.add("target/cucumber.json");
        String buildNumber="1";
        Configuration configuration = new Configuration(reportOutput, "HMRC");
        configuration.setBuildNumber(buildNumber);
        ReportBuilder reportBuilder = new ReportBuilder(jsonfiles, configuration);
        reportBuilder.generateReports();
    }
}
