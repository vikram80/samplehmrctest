package com.test.pages;

import com.test.configs.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends UtilPage {

    private By dresses = By.cssSelector("a[title='Dresses']");

    private WebDriverManager webDriverManager = new WebDriverManager();
    WebDriver driver = webDriverManager.getDriver();

    public void clickOnDressesLink() {
        clickOn(dresses,1);
    }
}
