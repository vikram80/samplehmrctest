package com.test.pages;

import com.test.configs.WebDriverManager;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UtilPage {

    private WebDriverManager webDriverManager = new WebDriverManager();
    protected WebDriver driver = webDriverManager.getDriver();

    public void clickOn(By by, int index) {
        getElements(by).get(index).click();
    }

    private Wait<WebDriver> getWebDriverWait() {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class);
    }

    public List<String> getElementsText(By by) {
        Wait<WebDriver> fluentWait = getWebDriverWait();
        List<WebElement> elements = fluentWait.until(driver -> driver.findElements(by));
        return elements.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public String getElementText(By by) {
        Wait<WebDriver> fluentWait = getWebDriverWait();
        WebElement element = fluentWait.until(driver -> driver.findElement(by));
        return element.getText();
    }

    public List<WebElement> getElements(By by) {
        Wait<WebDriver> fluentWait = getWebDriverWait();
        return fluentWait.until(driver -> driver.findElements(by));
    }


}
